# Traversy CI  

Repositório criado para acompanhar desenvolvimento do tutorial oferecido em https://www.youtube.com/watch?v=I752ofYu7ag&t=2s.   
Criando um  aplicação em PHP usando Code Ignitter.  
  
Log  

27-10 : 15:20 (Importação do Bootswatch)  
28-10 : 20:11 (Criação do controller para posts)  
      : 21:37 (Criação do banco de dados)  
      : 34:00 (Mineração do banco de dados)  
      : * Parte 1 Concluída  
  
28-10 : 10:00 (Validação de form e inserção de dados no DB)  
      : 16:06 (Deletar posts)  
      : 18:29 (Criação da view para edição do post)      
      : A aplicação agora possui read, insert, delete and update  
      : CKeditor -> utilizdado para aplicar formatação HTML ao texto  
      : * Parte 2 Concluída (Edição e criação de posts com ckeditor)  
  
29-10 : 01:05 Truncar textos na página inicial  
      : 02:29 Categoria de posts  
      : 11:54 Upload de imagens  
        O upload de arquivos requer a library upload com determinados parametros (controller/posts ln: 57-75)  
      : * Parte 3 Concluída (Categorias/Upload de imagens/ visualização destas imagens)  
      : Filtro de posts por categoria  
      : Todo novo model deve ser inicializado no autoload  
      : Parte 4 Concluída (filtros de posts por categoria)  

30-10 : Inicio parte 5 (inclusão de comentários)  
      : Comentários submetidos através de formulário  
      : Comentários formatados e visíveis do post  
      : Parte 5 Concluída  
  
      : Início parte 6 Criação de register/user  
      : 19:49 Flashdata => $this->session->flashdata('param')  
      : 24:58 Validação customizada "callback_nome_da_função"  
      : Parte 6 Concluída
  
      : Início parte 7 Criação do login de usuário  
      : 08:59 Query seguida de validação do usuário  
      : 12:39 Retornando apenas ID do usuário para criação da session  
      : 14:23 Criação de session
      : 20:07 Destruição da session  
      : 21:47 Inserir user_id na criação do post  
      : session->unset_userdata() # Função aceita array com novos valores para as variaveis  
      : Parte 7 Concluída
      : Corrigido session->userdata('name')  
      
      : Adicional 1
      : Validação de usernames (apenas alphanumerico entre 5 e 12 caracteres)  
      : Validação de senhas (apenas alphanumérico e !@#$&* entre 8 e 16)  
      : Nome do criador no post 
  
31-10 : Parte 8 Pagination e more categories adjusts  
      : Estabelecer limit e offset para a query dos posts  
      : Pagination quase funcionando - os links não estão operando corretamente  
      : Stoped at 09:36  
      : Problema da pagination concertada  
  
05-11  
        Ajuste na inserção de comentários  
        Inclusão da função de deletar comentários  