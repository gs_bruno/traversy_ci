<?php

class Categories extends CI_Controller{
    public function create(){
        $data['title'] = "Create category";
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;
        
        //Assim como o view post, essa função também irá gerar um form
        //Quando a página for carregada normalmente ativará o IF
        //Quando carregada pelo submit acionará o ELSE
        
        $this->form_validation->set_rules('name', 'Name', 'required');
        
        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header');
            $this->load->view('categories/create', $data);
            $this->load->view('templates/footer', $data);
        }else{
            $this->model_categories->create_category();
            $this->session->set_flashdata('category_created', "Sua nova categoria foi criada com sucesso!");
            redirect('categories/create');
        }
    }
    
    public function index(){
        
        $data['title'] = "Categories";
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;
        $data['categories'] = $this->model_posts->get_categories();  
        
        $this->load->view('templates/header');
        $this->load->view('categories/index', $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function posts($id){        
        
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;
        $data['title'] = $this->model_categories->get_category($id);
        $data['posts'] = $this->model_posts->get_posts_by_category($id);
        $data['title'] = $data['title']['name'];
        
        $this->load->view('templates/header');
        $this->load->view('posts/index', $data);
        $this->load->view('templates/footer', $data);
    }
    
}
