<?php

class Comments extends CI_Controller {

    public function create($id) {
        $slug = $this->input->post('slug');
        
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;

        $data['posts'] = $this->model_posts->get_posts($slug);
        $data['comments'] = $this->model_comments->get_comments($data['posts']['ID']);

        $this->form_validation->set_rules('name', 'Name', 'required');
//        $this->form_validation->set_rules('email', 'Email', 'valid_email');
        $this->form_validation->set_rules('comment_body', 'Body', 'required');


        if ($this->form_validation->run() === FALSE) {
            
            $this->load->view('templates/header');
            $this->load->view('posts/view', $data);
            $this->load->view('templates/footer', $data);
            
        } else {
            
            $this->model_comments->set_comment($id);
            
            redirect('posts/'.$slug);
           
            
            
        }
    }
    
    public function delete($id){
        
        $this->model_comments->delete_comments($id);
        redirect('posts');
        
    }

}
