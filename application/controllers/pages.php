<?php

class Pages extends CI_Controller{
//pages/view/[$page]
    public function view($page = 'home'){
// ----- verifica se a pagina existe
        if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
            show_404();
        }
        
//----- Caso exista passa o paramentro para a $data[title].
//      Que quando passada para a view, vira variavel $title no arquivo 
        $data['title'] = ucfirst($page);
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;
        
        $this->load->view('templates/header');
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer',$data);
    }
}

