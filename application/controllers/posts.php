<?php

class Posts extends CI_Controller {

    public function index($offset = 0) {
        
        //pagination config
        
        $config['base_url'] = site_url('posts/index');
        $config['total_rows'] = $this->db->count_all('posts');
        $config['per_page'] = 4;
        $config['uri_segment'] = 3;
        
        
        // init pagination
        
        $this->pagination->initialize($config);
        
        $data['title'] = "Latest Posts";
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;

        //Carrega header, footer + view do posts/index
        //É preciso carregar os modelos em autoload
        $this->db->order_by('posts.ID', 'DESC');
        
        $data['posts'] = $this->model_posts->get_posts(FALSE, $config['per_page'], $offset);

        $this->load->view('templates/header');
        $this->load->view('posts/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function view($slug = null) {

        $data['posts'] = $this->model_posts->get_posts($slug);
        $data['comments'] = $this->model_comments->get_comments($data['posts']['posts_ID']);


        if (empty($data['posts'])) {
            show_404();
        }

        $data['autor'] = AUTOR;
        $data['version'] = VERSION;

        $data['title'] = $data['posts']['title'];

        $this->load->view('templates/header');
        $this->load->view('posts/view', $data);
        $this->load->view('templates/footer', $data);
    }

    public function create() {
        
        if(!$this->session->userdata('logged_in')):
            
            $this->session->set_flashdata('no_permission', 'Não tem permissão para estar aqui');
            
            redirect('users/login');
        
        endif;
        
        $data['autor'] = AUTOR;
        $data['version'] = VERSION;
        $data['title'] = "Create your own post";
        $data['categories'] = $this->model_posts->get_categories();

        //Validation

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === FALSE) {

            $this->load->view('templates/header');
            $this->load->view('posts/create', $data);
            $this->load->view('templates/footer', $data);
        } else {
            //upload image
            $config['upload_path'] = './assets/images/posts';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '2048';
            $config['max_width'] = '3000';
            $config['max_height'] = '3000';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $errors = array('error' => $this->upload->display_errors());
                $post_image = 'noimage.jpg';
                die(print_r($errors));
            } else {
                $data = array('upload_data' => $this->upload->data());
                $post_image = $_FILES['userfile']['name'];
            }

            $this->model_posts->set_posts($post_image);
            $this->session->set_flashdata('posts_created',"Post criado com sucesso!");
            redirect('posts');
        }
    }

    public function delete($id) {
          if(!$this->session->userdata('logged_in')):
            
            $this->session->set_flashdata('no_permission', 'Não tem permissão para estar aqui');
            
            redirect('users/login');
        
        endif;
        
        $this->model_posts->delete($id);
    }

    public function edit($slug) {
        
          if(!$this->session->userdata('logged_in')):
            
            $this->session->set_flashdata('no_permission', 'Não tem permissão para estar aqui');
            
            redirect('users/login');
        
        endif;

        $data['posts'] = $this->model_posts->get_posts($slug);
        $data['categories'] = $this->model_posts->get_categories();

        if($this->session->userdata('user_id') != $data['posts']['user_id'] ):
            $this->session->set_flashdata('no_permission', 'Saia daqui invador');
            redirect('posts');
        endif;
        
        if (empty($data['posts'])) {
            show_404();
        }

        $data['autor'] = AUTOR;
        $data['version'] = VERSION;
        $data['title'] = "Edit this post";

        $this->load->view('templates/header');
        $this->load->view('posts/edit', $data);
        $this->load->view('templates/footer', $data);
    }

    public function update() {
        
          if(!$this->session->userdata('logged_in')):
            
            $this->session->set_flashdata('no_permission', 'Não tem permissão para estar aqui');
            
            redirect('users/login');
        
        endif;
        
        $this->model_posts->update_post();
        redirect('posts');
    }

}
