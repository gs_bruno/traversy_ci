<?php

class Users extends CI_Controller {

    public function register() {
        $data['title'] = "Sign Up";

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists|callback_check_username_string');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_email_exists');
        $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_check_password_string');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password]');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header');
            $this->load->view('users/register', $data);
            $this->load->view('templates/footer', $data);
        } else {
            //Encrypt password  
            $enc_password = md5($this->input->post('password'));
            $this->model_users->register($enc_password);

            $this->session->set_flashdata('user_registered', 'You\'re now registered and can login');
            redirect('users/login');
        }
    }

    public function login() {

        $data['title'] = "Faça seu Login!";

        //set validation rules

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');


        if ($this->form_validation->run() === FALSE) {

            $this->load->view('templates/header');
            $this->load->view('users/login', $data);
            $this->load->view('templates/footer');
        } else {

            $name = $this->input->post('name');
            $enc_password = md5($this->input->post('password'));

            $user_id = $this->model_users->login($name, $enc_password);

            if ($user_id) {
                //create session
                $user_data = array(
                    'user_id' => $user_id,
                    'name' => $name,
                    'logged_in' => true
                );

                //passing info to session

                $this->session->set_userdata($user_data);

                //Mensagem de boas vindas
                $this->session->set_flashdata('logou', 'Bem vindo ao nosso blog!');
                redirect('posts');
            } else {
                // Generate error  
                $this->session->set_flashdata('n_logou', 'Login inválido');
                redirect('users/login');
            }
        }
    }

    public function logout() {
        // Unset user data
        $sess = array('logged_in' => '', 'user_id' => '', 'name' => '');
        $this->session->unset_userdata($sess);

        $this->session->set_flashdata('logged_out', "Nos vemos depois!");
        redirect('users/login');
    }

    function check_username_exists($username) {
        $this->form_validation->set_message('check_username_exists', 'That username is taken. Please choose a diferente one');

        if ($this->model_users->check_username_exists($username)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_email_exists($email) {
        $this->form_validation->set_message('check_email_exists', 'That email is already registered');

        if ($this->model_users->check_email_exists($email)) {
            return true;
        } else {
            return false;
        }
    }

    function check_username_string($username) {
        $this->form_validation->set_message('check_username_string', 'Seu usuário contém caracteres inválidos!');

        //Apenas númerais, letrais entre 5 e 12 digitos
        if (preg_match('/^[a-z0-9]{5,12}$/i', $username)) {
            return true;
        } else {
            return false;
        }
    }

    function check_password_string($password) {
        $this->form_validation->set_message('check_password_string', 'Senha inválida: Permitido a-Z, 0-9 e @#$%¨&*');
    
        if(preg_match('/^[!@#$&*[a-zA-Z0-9]{8,16}$/', $password)){
            return true;
        }else{
            return false;
        }
    }

}
