<?php

class Model_categories extends CI_Model{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function create_category(){
        $data = array('name' => strtoupper($this->input->post('name')));
        
        return $this->db->insert('categories', $data);
    }
    
    public function get_category($id){
        
        $this->db->where('ID', $id);
        $query = $this->db->get('categories');
        return $query->row_array();
    }
    
}

