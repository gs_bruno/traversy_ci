<?php

// Sempre que um model é criado, ele deve ser iniciado no 
// autoload

class Model_comments extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function set_comment($id) {
        $data = array(
            'post_id' => $id,
            'name' => $this->input->post('name'),
            'body' => $this->input->post('comment_body')
        );

        return $this->db->insert('comments', $data);
    }

    public function get_comments($id) {
        $this->db->order_by('comments.created_at', 'DESC');
        $this->db->select('posts.ID, name, comments.body, comments.created_at, comments.ID as comments_ID');
        $this->db->from('comments');
        $this->db->where('post_id', $id);
        $this->db->join('posts', 'posts.ID = post_id');
        
        $query = $this->db->get();
        return $query->result_array();
        
    }
    
    public function delete_comments($id){
        $this->db->where('ID', $id);
        $this->db->delete('comments');
    }

}
