<?php

class Model_posts extends CI_Model{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_posts($slug = FALSE, $limit = false, $offset = false){
        
        if($limit){
            $this->db->limit($limit, $offset);
        }
        
        if($slug === FALSE){

//Se nenhuma slug for passada, será realizada a query na tabela posts
// Em seguida o resultado é transferido para um array -> retorno da função
// get_posts();
             $this->db->join('categories', 'categories.ID = posts.category_id');
             $query = $this->db->get('posts');
             return $query->result_array();
        }
        
//Se um paramentro for passado para a slug, o metodo get_where irá;
//buscar na tabela post pela linha que contem o valor
        $this->db->select('posts.ID as posts_ID, title, created_at, name, slug, post_image, category_id, user_id, body, slug');
        $this->db->from('posts');
        $this->db->join('users','posts.user_id = users.ID');
        $this->db->where('slug', $slug);
        $query = $this->db->get();
        return $query->row_array();
    }
    
// Inserção de posts no banco de dados
// Metodo chamado no controler posts após validação do form
    
    public function set_posts($post_image){
        //How we get the form values
        $slug = url_title($this->input->post('title'));
        $data = array(   
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'body' => $this->input->post('body'),
            'category_id' => $this->input->post('category_id'),
            'post_image' => $post_image,
            'user_id' => $this->session->userdata('user_id')
        );
        
        // insert => nome da tabela, array
        return $this->db->insert('posts', $data);
        
    }
    
    public function delete($id){
        // Metodo para selecionar e deletar linhas da tabela
        $this->db->where("id", $id);
        $this->db->delete('posts');
        
        redirect('posts');
    }
    
    public function update_post(){
        $data=array(
            'title' => $this->input->post('title'),
            'body' => $this->input->post('body'),
            'category_id' =>$this->input->post('category_id')
        );
        
        $this->db->where('ID', $this->input->post('ID'));
        return $this->db->update('posts', $data);
        
    }
    
    public function get_categories(){
        $this->db->order_by('name');
        $query = $this->db->get('categories');
        return $query->result_array();
    }
    
    public function get_posts_by_category($id){
        $this->db->join('categories', 'categories.ID = category_id');
        $this->db->order_by('posts.ID', 'DESC');
        $query = $this->db->get_where('posts', array('category_id' => $id));
        return $query->result_array();
    }
    
}

