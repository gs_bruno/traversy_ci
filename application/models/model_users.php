<?php

class Model_users extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function register($enc_password) {
        $data = array(
            'name' => $this->input->post('name'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'zip_code' => $this->input->post('zip_code'),
            'password' => $enc_password
        );

        return $this->db->insert('users', $data);
    }

    public function login($name,$enc_password) {
        $this->db->where('username', $name);
        $this->db->where('password', $enc_password);
        
        $result = $this->db->get('users');
        
        if($result->num_rows() == 1){
            return $result->row_array()['ID'];
        }else{
            return false;
        }
    }

    public function check_username_exists($username) {
        $query = $this->db->get_where('users', array('username' => $username));

        if (empty($query->row_array())) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function check_email_exists($email) {
        $query = $this->db->get_where('users', array('email' => $email));

        if (empty($query->row_array())) {
            return true;
        } else {
            return false;
        }
    }

}
