<div class="under-space">
    <h2><?= $title; ?></h2>    
    <p>Esta é a seção de criação de categorias para o blog</p>
</div>

<?php echo validation_errors(); ?>

<div class="under-space">
    <!--Make a post request to that route-->
    <?php echo form_open_multipart('categories/create'); ?>

    <label class="control-label">Nome</label>
    <input class="form-control"type="text" name="name" placeholder="Insert your category">
</div>
<button type="submit" class="pull-left btn btn-default col-md-3">Submit</button>

</form>