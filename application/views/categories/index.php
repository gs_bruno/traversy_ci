<h1 class="under-space"><?= $title;?></h1>

<?php foreach($categories as $category): ?>
<a href="<?php echo site_url('categories/posts/'.$category['ID'])?>" class="btn btn-primary"><?= $category['name'];?></a>

<?php endforeach; ?>

