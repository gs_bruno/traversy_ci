<h2 class="text-center"><?= $title; ?></h2>
<div class="row text-danger well">
<?php echo validation_errors(); ?>
</div>
<!--Função de CI que auxilia a criação de forms e especifica qual 
controler/metodo utilizará as informações-->
    <div class="row">
        <?php echo form_open_multipart('posts/create'); ?>
        <div class="form-group">
            <label>Title</label>
            <input type="text" class="form-control" name="title" placeholder="Add title">
        </div>
        <div class="form-group">
            <label>Text</label>
            <textarea id="editor1" rows="8" class="form-control" name="body" placeholder="Your text goes here"></textarea>
        </div>
        <div class="form-group">
            <label>Category</label>
            <select name="category_id" class="form-control">
                <?php foreach ($categories as $categories_item): ?>
                    <option  value="<?= $categories_item['ID']; ?>"><?= $categories_item['name']; ?></option>
                <?php endforeach; ?>
            </select>

        </div>
        <div class="form-group">
            <label>Upload Image</label>
            <input class="btn btn-primary" type="file" name="userfile" size="20">
        </div>
    </div>
    <button type="submit" class="btn btn-default btn-lg">Post</button>
</form>


 