<h2><?= $title;?></h2>

<?php echo validation_errors(); ?>

<!--Função de CI que auxilia a criação de forms e especifica qual 
controler/metodo utilizará as informações-->
<?php echo form_open('posts/update');?>
  <div class="form-group">
    <input type="hidden"name="ID" value="<?php echo $posts['ID']; ?>">  
    <label>Title</label>
    <input type="text" class="form-control" name="title" placeholder="Add title"
           value="<?php echo $posts['title'];?>">
  </div>
  <div class="form-group">
    <label>Text</label>
    <textarea id="editor1" rows="8" class="form-control" name="body" 
              placeholder="Your text goes here" ><?php echo $posts['body'];?></textarea>
  </div>

<div class="form-group">
    <label>Category</label>
    <select name="category_id" class="form-control">
        <?php foreach($categories as $categories_item): ?>
        <option  value="<?= $categories_item['ID'];?>"><?= $categories_item['name'];?></option>
        <?php endforeach; ?>
    </select>
    
</div>
  <button type="submit" class="btn btn-default">Post</button>
</form>

 