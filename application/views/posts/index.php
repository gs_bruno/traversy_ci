<?php echo $title; ?>
<p>This is the post section</p>
<?php foreach($posts as $post_items): ?>

<h2><?php echo $post_items['title']; ?></h2>

<div class="row">
    <div class="col-md-3">
        <img class="img-responsive" src="<?= base_url() ?>assets/images/posts/<?=$post_items['post_image'];?>">
    </div>
    <div class="col-md-9">
        <div class="text-primary"><?php echo word_limiter($post_items['body'], 100);?></div>
    </div>
</div>

<div class="info post-date">Posted on: <?php echo $post_items['created_at']; ?></div>
<a href="<?php echo site_url('categories/posts/'.$post_items['category_id']);?>"class="btn btn-info pull-left"><?= $post_items['name'];?></a>
<p><a href="<?php echo site_url('posts/'.$post_items['slug']);?>" class="info btn btn-default"><?php echo $post_items['title']; ?></a></p>    
<?php endforeach; ?>
<div class="container jumbotron">
<?php echo $this->pagination->create_links(); ?>
</div>