<div class="row under-space">
    <div class="col-md-3">
        <img class="img-responsive" src="<?= base_url() ?>assets/images/posts/<?= $posts['post_image']; ?>">
    </div>
    <div class="col-md-9">
        <h2><?php echo $posts['title']; ?></h2>
    </div>
</div>
<div class="container">
    <?php echo $posts['body']; ?>
    <p><?php echo $posts['created_at']; ?> by <?= $posts['name']; ?></p>
</div>

<?php if($this->session->userdata('user_id') === $posts['user_id'] ): ?>
<hr>
<?php echo form_open('posts/delete/' . $posts['ID']); ?>
<a href="edit/<?php echo $posts['slug']; ?>" class="btn pull-left">Editar</a>
<input type="submit" class="btn btn-danger" value="Delete">
</form>
<?php endif; ?>

<!--Aérea dos comentários-->
<div class='container'>
    <h4>Comment Section</h4>
    <?php foreach ($comments as $comment): ?>
        <div class="container well">
            <h6 class="text-info"><?= $comment['name']; ?></h6>
            <p><?= $comment['body']; ?></p>
            <p><?= $comment['created_at']; ?></p>
            
            <!--Deletar post criado pelo usuário-->
            <?php echo form_open('comments/delete/'. $comment['comments_ID']); 
            if($this->session->userdata('name') == $comment['name']):?>
            <input type="submit" value="Deletar" class="btn btn-danger">
            <?php endif;?>
            </form>
        </div>
        <hr>
    <?php endforeach; ?>
</div>

<!--Realizar novo comentário-->
<hr>
<?php if($this->session->userdata('user_id')): ?>
<div class="container">
    
        <h3>Add Comment</h3>>>
</div>

<?php echo form_open_multipart('comments/create/' . $posts['posts_ID']); ?>

<div class=' container text-danger'><?= validation_errors(); ?></div>



<input name="name" type="hidden" value="<?php echo $this->session->userdata('name')?>">

<div class="form-group col-md-12">
    <label class="control-label">Comment</label>
    <textarea id="editor1" class="form-control" name="comment_body"></textarea>
</div>

<div class="form-group col-md-12">
    <input type="submit" class="btn btn-primary" value="Commentar"></button>
</div>
<input type='hidden' name='slug' value="<?php echo $posts['slug'] ?>">
</form>
<?php endif;?>
