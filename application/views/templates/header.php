<html>
    <head>
        <title>Traversy CI</title>
        <link rel='stylesheet' href="<?php echo base_url('assets/css/hero_bootstrap.css');
?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
        <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header" style="width: 60px">
                    <a class="navbar-brand" href="<?php echo site_url('home'); ?>"><img class="img-responsive" src="<?= base_url('assets/images/blog/logo.png'); ?>"></a>
                </div>
                <div id="navbar">
                    <ul class="nav navbar-nav">

                        <!--                    site url habilitado no autoload.php;;
                                            base_url => /traversy_ci-->

                        <li><a class="h5" href="<?php echo site_url('home'); ?>">Home</a></li>
                        <li><a class="h5" href="<?php echo site_url('posts'); ?>">Read Posts</a></li>
                        <li><a class="h5" href="<?php echo site_url('categories'); ?>">See Categories</a></li>
                        <li><a class="h5"href="<?php echo site_url('about'); ?>">About</a></li>
                    </ul>   

                    <ul class="navbar-nav nav navbar-right">
                        <?php if (!$this->session->userdata('logged_in')): ?>
                            <li><a class = "h5" href = '<?= site_url('users/login'); ?>'>Login</a></li>
                            <li><a class = "h5" href = "<?= site_url('users/register'); ?>">Register</a></li>
                        <?php endif; ?>

                        <?php if ($this->session->userdata('logged_in')): ?>
                            <li><a class="h5" href="<?php echo site_url('categories/create'); ?>">Create Category</a></li>
                            <li><a class="h5" href="<?php echo site_url('posts/create'); ?>">Create Posts</a></li>
                            <li><a class="h5" href='<?= site_url('users/logout'); ?>'>Log Out from <?php echo $this->session->userdata('name'); ?></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">

            <!--Flashdata funciona de forma analoga ao $data['param']
            set_flashdata('name', 'message') e será armazenado na session
            -->
            <?php
            if ($this->session->flashdata('user_registered')) {
                echo "<div class='alert alert-dismissible alert-success'>" . $this->session->flashdata('user_registered') . "</div>";
            }

            if ($this->session->flashdata('post_created')) {
                echo "<div class='alert alert-dismissible alert-success'>" . $this->session->flashdata('post_created') . "</div>";
            }

            if ($this->session->flashdata('category_created')) {
                echo "<div class='alert alert-dismissible alert-success'>" . $this->session->flashdata('category_created') . "</div>";
            }

            if ($this->session->flashdata('n_logou')) {
                echo "<div class='alert alert-dismissible alert-danger'>" . $this->session->flashdata('n_logou') . "</div>";
            }

            if ($this->session->flashdata('logou')) {
                echo "<div class='alert alert-dismissible alert-success'>" . $this->session->flashdata('logou') . "</div>";
            }

            if ($this->session->flashdata('logged_out')) {
                echo "<div class='alert alert-dismissible alert-success'>" . $this->session->flashdata('logged_out') . "</div>";
            }

            if ($this->session->flashdata('no_permission')) {
                echo "<div class='alert alert-dismissible alert-success'>" . $this->session->flashdata('no_permission') . "</div>";
            }