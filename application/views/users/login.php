<?php echo form_open('users/login'); ?>

<div class='row'>

    <div class='col-md-6 col-md-offset-3 well'>
        <h2 class='text-centered under-space'><?= $title; ?></h2>

        <div class='form-group'>
            <label class="control-label">Name</label>
            <input class="form-control" name="name" type="text" required placeholder="your name" autofocus>
        </div>
        
        <div class='form-group'>
            <label class="control-label">Password</label>
            <input class="form-control" name="password" type="password" placeholder="your password">
        </div>
        <input type="submit" class="btn btn-primary col-md-2 col-md-offset-5" required value="Entrar">
    </div>

</div>
<?= form_close(); ?>