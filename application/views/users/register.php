<h2><?= $title; ?></h2> 

<div class="text-danger"><?= validation_errors() ?></div>

<?= form_open_multipart('users/register'); ?>
<div class="row">
    <div class="form-group col-md-3">
        <label class="control-label">Name</label>
        <input name="name" type="text" placeholder="for login" class="form-control">
    </div>
    <div class="form-group col-md-3">
        <label class="control-label">Username</label>
        <input name="username" type="text" placeholder="to display" class="form-control">
    </div>

</div>
<div class="row">
    <div class="form-group col-md-3">
        <label class="control-label">Zip Code</label>
        <input name="zip_code" type="text" class="form-control">
    </div>

</div>
<div class="row">
    <div class="form-group col-md-6">
        <label class="control-label">Email</label>
        <input name="email" type="email" placeholder="your@mail.com" class="form-control">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-3">
        <label class="control-label">Password</label>
        <input name="password" type="password" class="form-control">
    </div>
    <div class="form-group col-md-3">
        <label class="control-label">Confirm Password</label>
        <input name="password2" type="password" class="form-control">
    </div>
</div>

<input type="submit" value="Registrar" class="btn btn-primary">


<? = form_close(); ?>

