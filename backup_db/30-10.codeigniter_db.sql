-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: codeigniter_db
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'BUSINESS','2017-10-29 12:53:28'),(2,'TECNOLOGY','2017-10-29 12:55:16'),(3,'GAMES','2017-10-29 12:55:49'),(4,'CINEMA','2017-10-29 12:55:49'),(8,'MUSICA','2017-10-30 17:16:54'),(7,'PHONE','2017-10-29 19:21:30'),(9,'PROGRAMMING','2017-10-30 17:17:25'),(10,'GUITARRA','2017-10-30 17:18:12'),(11,'SAFADEZA','2017-10-30 22:14:26');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `body` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `post_id` (`post_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,10,'BRUNO GONCALVES DOS SANTOS','brunogoncalves.santos@gmail.com','<p>&Egrave; mesmo</p>\r\n','2017-10-30 14:05:03'),(7,18,'BRUNO GONCALVES DOS SANTOS','brunogoncalves.santos@gmail.com','<p>Essa carta n&atilde;o &eacute; muito boa, essa &eacute; melhor</p>\r\n\r\n<p><img alt=\"\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFhUXGBgaGBcXFxodGBgVFxYXGBgXFxUaHyggGholHRcWITEhJSkrLi4uFx8zODMsNygtLi0BCgoKDg0OGBAQGysdHR0rLS0rLS0tLS0rLSsrLS0tLS0tLSstLS0tLS0tLSs3NzctLS03LS0tKystKy0rKystK//AABEIAQwAvAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAEBQIDBgEHAP/EAEMQAAIBAgQDBQMJBgUEAwEAAAECEQADBBIhMQVBUQYTImFxMoGRI0JScpKhscHRFGKTssLwBxZTguEVM0PxY6Kzc//EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACIRAAICAQQDAQEBAAAAAAAAAAABAhEhAxIxQRMiUXEyBP/aAAwDAQACEQMRAD8AC4c7G2oDNtHtH02pnwjDd5dW0zsM86qx0hWaYO+1JsEp7tTMadPM0dw/iCWbyPoSskBmgHQg7eprlfJ1JYOWVZyAheSBC5iSBA0nSrv2S8QrgXMrNC66lgQsATJ8RioWeK2EXKqpykm4xJynMhmdIYKfurg44AbTHIz27puB80TmcXCpG24MetNULJGwtw6+IyJBkwVkiZJ6gj1q7AoWzsWYFEdx4vbCKWKzPhkc4613CcaRAuTKoW3kUZyfD3hdc8+1qx6b0Ng+LrbYsCrMUdJJEAOpUmOuoj86l5Kp1kJ4hhWJtm0bhDozQSSVytBBI0ImIPPWhWwd+EOV/GMywd1PPfQaVY3G0CXFMHPlGjxBUkwpGyyfZFDnjFkqiuiNltG1o8AghgpIg7ZttQYq4kMmQ62nZmuK63VQqeQKlp9dB8aFbENvnaPrVPifHVu55yLna2x8RJz2rfdyOgIG1Lhik+kvxptEoYJjGn2m+Jq9MSTrmaR+8eemtKreLXky/wB8qtGKXLlka7mf76VLTKTGCuZ9pvTMamHYH229JNLbeNQR4hA8x8al+3qJlgeQ1G9JplWg5r7eyWaR+8YobE3X0GZvtHnVP7cuXdZ6zVJxQO7g++nGyXQUrvzY/aNcuXj9NviaGGISRLL8RUP2hPpDnzp0x2qCmvt9Nj7zXwvNtmb7RoXvkHz1+NfHEr9JfjTFuCu9b6TD3mmHC8L3ge49x1t28uaDLMzsFRFnYkneky4lPpr8aY8N4vaRbiMwNu6FzZWGZXQ5kuLOkg8jvTJG3EuEm2HKs3ybBLilpgsAysrCMymelBYWyrZiwmDAneMo0+JNG8R40LqtkB+UYO7QBmKrCgKJyrz3qvhjjK2g9r+laTeSlHGRdhB4EEz5e81K5bAtX5iYtH0i4dulAcPuQi6axp6SaIe7Nu99W3/+lSo+xW5ULgo8/ia6bfmfiarV9aLtCuxwXwwUmDEHqfiajajMA7NlOh12nY0bdShmtiRO06+kwYoUEN2D4pHRihYkCYM6EHYihL1xhzJpsbYk2yZgnIw2I5D0IpfesmYik4EuymZII2PIn2TzHpRXcqDqPvNcwdoBgxEgbjy2p1awYuWhHtrt5rvB86aiCF17CqCMokESNT7xvTPhvD7bNDLMgEann76jgLcjKRqNp6HenXDsNBUxsIq4xRdGe43g0t3IUQNeZ5HzNDWkGmnOTqdfLQ7VpeNYEE5v3R+Jml+AwU6xMcuvQVLjkaQPewaRJG+sS0D76HbCKNx952+NOMZaIMMNuXn0oB0JPrS2odA1vCqdI/H9aLThaRMdeZ/WrrFgTy86vuXDtyqtiHQnbBL0+8/rUP2Ben3mnK4bmdfIbe81C6eZ+6lSG4Cz9hUbj/7H9aHv2lG3UczRuIuUG529R+IqZJURQ1wewk6QNPdT3hKjK2g9r+laQWtVUDfT12p7wcAI0n539K1xNG26jL4P2F/vmavbW3ejaLf89V4Q+Aa/3NXTNu7G8W/KPlDWy/oxrAuNkxPI1fYMVO0SnhcaGuXbUarqDzrrWSKC1UiGEH+9jUr9oHxKPVenX3VDh97dW3+8efmKMezzBkc46dYqikJnsS0DQE6Dp5Uww+EBDKyyYj38mBom5h1JKz9Vv1opARv7a8+RFPaLsT4LBFWYHY6EeXI03wdgAjrsfNf1os4bNDqOc+h6Grlsc4jWPQ0JUCRS+DAYHpt6dKOtWtCw0G4q4Jp4h/7pH2o4uLNo2w0OV5fNX6R6eVVhFNjXEWJG3lQVzDFAFXSPET+FX9neIC/h1afEsK3r/wAiDV+ITxDc7E+fl6UqTyKxVfwpIk7nXXp1oV8MoXTc86f4yzO/tHcdByqg4ULrcOVeQ5n3UUikKcNhWJ0FFGwq+bfcKLu4gsIVcqfj6nnS3G3gNFoKKcXegUqu4jzq7EYoAFTblz84k6DyUaUAqEmspDbJHWqb5ge8fjTduH5QNZY8hyoHiVjKNeo/EVLWGQw/AtKqGGuUQR1p3wjxKx/ejboq0hwd3QDY5RHrFOeF3SqkE/O5H91a4qyab8Gb4engHp+Zox/+1e+rb/nNCYIRbXn/AO6JVJS6NSSLf89axXsjNv1A7V9ojdeh/I0RbujaMy/RO49DUUw5G8+/nRVpD81Mx/CvSUDl30CC2pIKsRHIj86aYe5lEzJHP8vShxiQDDoB+8N/hzqzv1QzIII0O6t5eR9aW0pTQxCow0WJ+41bY5T8elKDi8okaD8J6eVMeHY1HMHfl0P6GqjF0HkjY3wloqfI79KmzAmpYUHUA/35+dJu0vGP2e2W+exhB58291TIpSKe0/aLuT3NqGvEe5B1PnXn+MxBuMZYtrJY7u30j+Q5VxnaMzEl7pJJO+WfzP3Cr8BZ3PLQfCuZysfJdwfilzDt3iaxAdOTW/LzBr1DhWNt4i2LtttCI8weh868wu2oOYcif+RRHBOKfsd0Op+QuEB1n2T+RH4VUZtcjo9PHhUmJPLzNLL1osc9zU8hTBrqkAgggiRFD3kBGpyyee/pFbVY1NIBuS3oNgKExClAYAzHYnl6U7w1heWp9RVOMw4G9JxY1qKzKrgGbU11cOFOuvpTq7a8NK8RbIEKCSdzyA9alxK8iPnxBAgeGlXEHn4j8RRBuONCaFxSeEk+X4iiUfVmTnkLwmir6D8Kd8IIKtMnxbj6q0kwtgsojoKbcMtMFOQiJ19cqzXnpqzfaxJgz4F9PzNG4NJV5gD5P+c0DhYNsf3zou0xy3NM0C3pzbxnpWkH7oh4i2Fm+1vwkADkd5HXWqWuq5gET1X8wd6OwQa4ArJA6EzHvp5gezi7wPcK9hNQ5PNlc+DNWsEW318/+Kru8JdSSi5p5awfXWvSsLwBVEmAKpxeJw1odSKh666RUf8AO32YLD8DJ1y5D0GopjY4LGmX4UybtTZLZUUBvOpNx25IAVYPMcvUVPmfCNV/mS5Z3DYMxmIIbb16T515l2txZv4soPZU5BH/ANjXpfGuJBLL3R81SffFeRYaYa6T4mOVfrNq7e7b31z6sm8GsY0TuKWuMRsug9F0o/AIco99WYLDZUOmwAphhMNt6fiQKzSNVHsFezp8aWXxlJkSp0YdR+orRYizGlI+LrE/WFNqip8Gs7FX2a02HzeyQVbn3Z/PlTW5w+4rEhif75msf2DxMYhROkOPcdR+Br0Jsfk1Kgg/GujTng5ZQ3CoWrw3+A/WuNeubFT606s8ctTDpFMbAw9z2SPurTyL4Z+NrhmOxN65sNBSu+C5iST91ej4jgSkaQaQcQ4GRMSPSqjKDIkpoz+DwCwS0afH0qji6KLTAKRMan6woq/gnEKNFHXmeppdxR8ttlgnbxA+H2hyo1ILY6CGrlJluEByqeoH4Ux4Y5AYZfnf0LSrBLKrPID8Kb4HGA5oGmb+ha8JXZ6zcayZzBewp5c/jTTBoHzBTB+Tg8wc5pXgmGRfTl6misJiMod9Tl7sx/vNdGmrmv055P0Zu+E8PUak5jprWvwWHUCvP+B8YDAGCJ5HStfguKDrXoa0ZHJpTiX8dVsuk15zxWZ2P516vbxKOIMUBxDgNq7qImsIyo6tyaPI2vLs3i9VijcDiLbGFBmNIY6HyFajH9jDuAR5qaCfgT2/m5vrD86tNBtE/a27mwlwAQWyjaNyBNYlE8YUezbygepOp+P4V6D2hsZ7LaRGUxryNYfh+FjPIO/9U1nNZCKG9iz4fU/hNG4S105j7gymr8GBEHlJ+M1fhAJPlpvprr+VNRNgTFJ4j0zfgazPGbc/Ek+4GtdjXEknqT8ZrM8YZQrHqNPzpTRMuAXsWYvg+f5GvQcZaMTsNJJ/CsR2GtDvQSRzOpjlH516DdwfedSBsBNaaf8AJhQluDMp00nrE1PBkgjKoX0p1h+zdxyAy6dKc4LseinMSfSabkiqRPgL3Do0nzptibEjUVdbt27YilvEeKAbVCtvBEnFCbiuAU1iOPYNFtOVGsrr08a1quIcUBrIcbvh0YAkQVJUjfxDnXVTUH+HK2t6BrDeADqBRnDrJho18X9K0NhrXgUnoPWmnDbZytH0v6VrxUerJYM/gz4F/vnRuGuBRcJnQW9vr0uwmiCibIBS4C2UEJJ6DOa3jiSOd5i7CrKpOe3dAPPPJNH4fixHiLQOu0+k8qSWMPbPil1XqyDUD6OulU4rE940kQBooHIfrXqrVVZPP8Tv1N1w7tATsQR1H604tdoCK8/wuGVSrZpYARamIPUn76ZrjNYbKI3g7U3pxlmgWpKLo2lvtSauHadPnKDWWw9u3M5t9RPnVGM4TdY/J3F18wKz8UDRasujW3OM4dt7Sn3b1BMbhjtZUe4VjLWAxFskBGIA1fUz9UcqB4xw3EF81ssFIkkk5Ujf4jYDnUyhBGkZzZ6D+3YX/RX4CunFYTc2U+ArznE4xrQW2rsWIlmZTInYAcqH/bjqQSxjnOUeICAo9d6W2A9+oemPicGd7CH3VC7cwbb4e39la8xs8Tu51zPpIkHaOdMuG8WuXmi2FOVoZTOqEwHVuo5jzo26Yb9Q3lnG4VPZsWx6KKNXtEg2UCsViMNcJXugzSYYRsOs127wi9mDZwqj6R1A/OmtOBD1JmyftN0+6gMR2pPU0lzouhYMeopbjLpJ0Rj7vwNaR0Y/CJasqGeL7U6mlj8SN4wtwT0mgO5edCT1VhGnkedfPw2CGEidQR7tCK22wiYKWpNnMXcyNBuEHoU0+NBY4IyFwAGDKNJ8UnoaZXMSCIugOOuxHoaBxrWhaZUzEkgifJhWOrP0aOvT0GmmTsscojoKa8KueFpMeL+laTWk8CxvA/CmvDF8J+t/SteIqO92IcEoyAnp+JNWF8tq8diFQj+JFQwmqLrsPjU70d1enchP/wBK0XJHQsuIzsCzT4RufM03wPDjuWURrMg/dSyxbE7f+qYWm1H96V26fJk1gaWcgkAETu3zm9elGYbCoQYTQ7zS5cWZ0iOlHftpOk6eVdS1KMXpJs5jLKhlA0036AeVWogBAzHqfT0qu5dHqetdw9sMZY6c/PyqvIS9F9BHDcbdaSrMF5edHWsdeI9vTzAOvv50tbHMmuTwfAgeVXnjdp9TbZTpqu36UmxRh0DcS7W4i0+U2x5Eqvi9PPyq3DdqblwEoisehQCDPM0yxNnD3PazZTuGWeXIg70uXgWGUeG42/ziwIn0rJtp8GiiuzmE7RO5yPZt59coygho5bb0X/1O+B7NtDGoVBp5SBVGAweHsyQzEn50En0k1bjMRa0IV2IBPTQeVUvwTSKlx9y6cveeL6O1A4y+w5k8iDVq46BmS0ELcyNT560ImKMlX8SnXzBrQnYrO27SttIPMHb3VMWGXwkEjlrNVG6o291F4fHTp+NJyZqoRRyAVynQ8j0qJZ0BC+IH40WxO8CKiqD6PwrNtm0VFCp7oO6D8KB4jYVVkHWQYPLUU7xNsdKT8Stwh9V/mFYT4Ze6y3DkQsdB+FNOEOArafO/pWlFkDKvkAfupvwxQVOvzv6VrzbNeTO4Mwg00j8zV7j5O75i3/OaGwq+BfTr5miY8F30T+c1quTLoBt0dhrJnMRoOtCKRRlh+pmuxNIg5qCTzmiMNd++vmszVZs5aVtjwXXWMyK4uJIqK3I51RdYdau6G2miV/HOREk+VO8DjLFxVHskaHTkR09azA1pvwzBE+KQBpvzkxWylg56e7BrOGKkaupGU84EjSmFy1aIbxJ82NRyrN4LCsqjSYZgdPOnl3A63NI0H4VDkjRxZVirFvKoDoNp18qVY17YJPegaBdBPm33UVjeHapp0pViMIGZRO5c/eq/hNVFoTT+i/ivFBcPgBCgaT/egoBGY0Zj8MVPUEmI2qOE9K0bSRkotsGJNX2UNRxGhqVi5rWPkybbcF6Y0ijExmkzVNuwhqm/ZjatU4sxbkgu7ippdxF/kzp0/mFVmaji7ngIB3K/zCo1EtrNIzYTg9VUASQPjpTfhdww0gDxbHf2VpThpEEchtO+mtPODurKxyj2uf1VrxbOwyWG9hfT86IsOviDKSGAEAwdGJoXD+wuvKfvo/h0ITdnW2AwAZMxfNplDRPMnfatjKytreHk/JvI38Y0qSLa5W3Gn0hvTG7aw6uT4GU3ky/LL4LLoXhlQ7oYQnyqLuqq2VkGiiBdByKcSwfKcxiUgnfeaG39GmvgOL6ASA8acx+lVviFI1V/tDaaLvYe0oYI6SqiV7yFPjuK2VsxloVCFmSGmOVD44p3jZcoSABlaZXLvmkw08tPSlvkuy6T6BWu2zJCPHLxCuWzb5o32hVZIIMbch+NRAp75PsmqLg1rcI/2hRWH4iE0AePVT6HWgI3moMapSl9JcY/B03HXA3aN/mb9dqvtdprzCczzz0TUfCkRbw8j1nz8q5h7TSD5T0Ea9aje/pSirHX+ZbjQWZyAdPYkee1QPEIIMMSJO67TqRpS/8AZVgNm8MajcmeQrt0hZbYQRl58pB/WpWtLpleNdoLu41CYYOfetRW9aIkI++ozDal7mR/cVO3pPWqepN8sFFdF967ZJPguR9cVGzcs6RbfePb1mKgQIiq7I8UiB+Rpb2JxyMXxFtdkuD/AHDeK6MXbM5lcx0Yc6AFw+87mRt+VQJ1841/Kmpy+g0hkHsMcq27hJ2GYTVmKwlkKZRgZA0uK2VoJUMAdJAoCxdGV1bOA65c1tgrjrlYqw12MiruEcHw9hLl2xdvu2X5SzcKZSssBnVUl8uYNmBAmdeVVulXLJxfBTh2yqCekU24Tc8Lebf0rSa8YAid532MelN+D4hghGVd/wClazSssQ4QeAH1/mNabsTwi1iLtxboYgKpADRrJ50gwinul0BEfma2n+Gj/LX9P/Gv4tVJ5M5RpDteweCIJK3J/wD6HX7q4exOD+hc0/8AkP6Uwx/H7GHdLd1yGdgoAViATsXIEKp2k0di74S29xz4Las7EAmFQFiYAk6A7VdGYmTsPhIkLc1/+Q79dt6qHYrB7Zbn8Q/pTrAcUV1kBlXKrBny5WV5ylXVip22mRRLMuYgsAYmCRMdY6UqHb+md/yLgojK8fX/AOK5/kbA7Fbn8Q/pTfG8VtoYObVlVAMvyrMpYd3LAHQHeNqIsOGCknKXEhWgPtJGUHcQetNITbEI7CYE/NufxD+lc/yHgfo3P4p/Sn+KxK27T3dWVFLHJBJC7xqBPvq2zdUotw+EEBvHAIBE+IyQD76eBWzN/wCQcED7Fz+If0qQ7A4L6Nz+If0rRPj7WYp3tvOACVzjMA2imOh5dakLymAGWSYGo1ImYjeIPwPSkFszv+QcDoMtz+If0rjdhMFOq3ffcP6UzxPG7aO6MlwZDblsq5YumEb2sxEiDppRN7F21KhriKXYqgLAZmALFV11aATFFIab+iP/ACBguS3I/wD6H9K63YTBfRubf6n/ABTtsUqlQT7TFQRBAYAk5jPh0BqHFOIJYttefMVWJyDMdeYHOBLHyUnlRSYK/onPYXBjYXP4h/Sof5Jwf0bn8Q/pTrHY9LSB2MglQMsSS5AUiSJGoOnI1G5iU79bE+Nkd+UBUZAQ2sg+MRprrRtC3YqTsTg+S3P4n/FRPYXBj5lz+If0rRFwDBKyN9Rp6127fVhOZYmJDCJmInrII91CQORnF7DYKPZufxD+lfDsHgjrlufxDz91PyhFX23HLlSoZ4ZfsgOy8szDXoGIH4U04LbGRtZ8X9K0FiLUs8CSGfn++1OeBWGyNy8X9K1nZukxDw5ptp/fWtj/AIbpF+8pO1tfvY71i+HGLanbflOsmth/hmGF+8N/k1/mNCXsE36mjx/Zx3vB1vsttnZ7ig+0HQK1uCIyHKp6iDHtU0y3ct1QwTdbLATkGSFJWdYYe8Uju2sUS5zYnV8Tl8WmTQ2I9+3lM1Vjf2/vDlW+Fm2JXKf/AD2CxGug7o39I5czW5zF13ssSjqrW0DmSiKe7BysrOin2HYsCYAGnvojE9nmZXQm06sUcZ0llYNbZhm+ie706SKHjGgqAt6FvXBJK62Rf8GaTJPdncyYG060x4imIN3wFgmawVKnw5BcH7SrjfMUnL55Y1pIAQ8GVrzvbNnL3ttsgXRe7sm3lMaTqPcIqNvs4wQ2jcXI9oWyQvjtuqMivYb5ntA9RrQ2Gw2JtYCxbtpeF0KA4LDOrZG1clpYZo2PPXSa5et4osc1y/aRrl1WuMQEt2ThCVu7wuW+FIO/LY0IBvZ4Sf2e7Zm2Ll0HMVVgmYqEzZSSRoB91Su8Out3GZrcWMrBYYh2CMhzTsuViRvDAHlVOCuYj9kF3KxvNlY2wwJy+EMttmIAJALDX53KoKuKDKR3oXvLTDvCp+QP/ft3QCflB4o31yRzoArsdnAqEI6FXtW1VsskFLjXFYEbp4o3mFFWYDgLI6XM6yrXGbKpAYXHuOVy7aF9G0I8XJiKB4ZhsWuGtoBdQpathQuWAoswyEa/KZxI30iDE0xvYW73FjW+zC7bd5aLkGQ4OWJUTqBOgnlTAm/Cg165cuZWVlQKIOZWTMM45T4tOkUJhOzuU2Wm2XtXQxOVvlFFm5ZGYknxgXJkc1FGcJt35m+T7BFySI73MfHZImFy6/DnNLcAmJIUl3e3Ny2Wt3AWYIpFq+s8maS0c8h2DUCLcDwB7MHPbPiUxkIXw22SMvTxDTyphiOH96befKbSK0oMyzcYBQQVI8IQ3BH79Jb6Y+HB70vlBV7bIUzdwAyqpaQe9BO0a6GmmLsXsmFRHvn5Yd8wb5Tu2t3ZDkAGA5txGu1IYLY7OXRbCm5bbKqKMyk5Vs3c9vL5xCn6oNE8P4M1u6lwurBBfUHL8owv3LVzxNsYNuPP1pdYGNFsSMQWa2maWUEXO8ZSZJ+jlJCkTHWahhWx/wAmzd/OdQ4lMvdnBPnI+bP7QE1Mx6ZpeR0Mcb2YLo6518XeEFlkkXWDFX6gEae7pVD9mSWY5kym5dY2ird2bd63aVgVUjxg2pDf/I3Wai+NxCYLENdNxHUZkJK5guVNjJ1zZxr8BtUu4xecgPday3eZDI79WNu2bbEne2HF6J+ks+EUhMfXiEAmABAk6eQ1NTsfjWbxWFxbWmW6C7/OVYKN47bIbQ5RDTt+FaWwc2pVlMkQ0SYMToTodx+FLjkd/Dxh3i4/XO4/+7Uy4TxAhWgT4v6VpTdcd5c+u+/1zRfDrmUMCPnf0rWe1Wbb3Qpwg+SX++ZrZf4ZCL14DXwLP2jFYrCNCL/z1Nbb/C0zfv8A1E/mNOqYpP1HKtde/i8l4gWbwJJuE5U/ZlZkCbauT6V9Z7TXQq27ardudwzAFjme5btJcymObAsB5gedakKNYA13PX16++o38IrrljQiNJU+UMsHlWhiLMVxZ/2NsTaKeI5rZIOU2WcBXYdSuvvpPjeN3LQxElbgLYlU8ZDK1vCm8ArQYXwwOYkGtZpGXKAAIiNIGkR00ruRI9kc+XOIP3UXQ+RZwbihvOyFVGUKVg5pUxrm9+280nfjzXXtouRYvopk/JulzD4jw3JHshkHqQBWtELsAPQRXDZU6wIPKNDzp4JRlv8Arb3GH/jb5AkZ9Nbt22YBHsnJIPMEdK5he0N3u7lwhDbtLYdlzEuLLx31zN87IpZtvmRWp/ZgdRHwqu7hEcZWXTTQSJg6AxErv4ToeYpYGB4ziz2sOt1wmZokEkKCwJAHnEUPwbjpv3AjKqhredYMzohIJ5EZ9j5RNN7iT57VO5ZGwA5ToNh1pFCfhHGjea6sL4BMgyD4nWD6ZPvpWnadwiuUsx3eGuQrHRb942So0iViZ861lxAOQ89P7mqrSKZGRfgNgef40JkvkQ2u0Ja4barbnOqAljHie4mvMxk++o4btOXfDplX5YqrQ2qllv8AiHlmswPrcorSlFnYfD4muiys+yPKB7x6UxoQY3jDWrjKVEBQQZJJJ3mPZjfbYGqMd2ie3nlEhReytJy3HtdwyhD+8t19ORttuK1GQbwJ6/376rRB0BHpp8PfQKxC3GjLh0si2Lz2Q7NmUFRKtckQJmPWrsFxnNdZWFtUTOHOeGUplytliTbYEw07R10dG2sHwjXfT8apBncCTuYG3IbajypCWSwnWDUgNRUWA519OooGeKYjRrhBj5RxtvLtNOOzq2u7bOJOfQ+WVaS426ud4X57zPXOaJ4Ywyt9b7sq1mrNmKMP7A/vnW1/ww/7976i/iaxODByj++Zrb/4Yj5a8R9Bf5jWnZL4Nlxa3cTPfN7LaRcxTbRVYkTyklD/ALfOkGG4jctth1vu3yNk3rkXcxutcZLdsP0XM5Ou0CtJi+M2RKkO8P3ZhJAYAsZnTQCT5RvNU2OKYfwkn2yqy1sj2gSueV8IIBidNvWqMgQ9pSZItqQDekhjGSwQrMJHNiVHpNRs8XYDGYjRltLCICSua3aL79SzBdPo13A9oUezbuNlV2yyO6uZcrqboA8zb3MkBt6Nucew6ozljlGbQW2kxb7wwkeKEgn4bmpso7gOKm5cCZQYzK7AnR0VczAH5hYwJ332pTe7TFDcKW8+rwM5jMt23h7agR4S7sR6iab8V4itnN4TmNt7k5fCAgAGcyCDJUf+qjguJ2T3Ykd47ZYCMPlFJnkSFzAgE+etFAgSx2kDHKAoPelAM247xrYI03lHnloOtQbtEXCSgVLl22iOLkMcua7d1gjwLb16zFG4vimGFxrDgiVEtl8AL5j3feDUOYJjT1mucPxmFUlUVtQjlivtNclEQA6lyFbSNt6AArHaksF+SAdhbIUtENcUvlYnaFg/7hTVUNy6lxL5CAgFF2LIxzAyN5OU+Q9KjgeJreuKLUG2bfeFipDAliluJ5EK245UPisS2HYzZCYcNrcBnVgCGImSXuPl20KydGlQLBbnalpI7pYzMoOfQxeFpDMfOOaPq1Be1AhmW2PCmdobk10osD5xyI7kDXwwJNNRxbDjwkkEGCGtsIIXPrIEQCPQmN6Fu8WQX0tlYRkBIa22cXXvC3ZEclMXDMfNBmJpiKrnaZQrnKsLnAJbRmtg5xG4EiBzND8TxFwYfCWBcJvXrtoZg+ViqK1694t4KWyv+4U7uYq2HFs6sxjRZGYgkZzsCQCaDvcTVL1wOFy2kWCFJud4Va5cAAnRbYRjA+dFAhbhuO5BiL7uNXdUTvJtqMOgzhT1L6VfiO0hXNNtYTNPiOpTDi84XTWGZE/3ijrfGMMVkaJrBNsgNoDK6ayCI5mq7fG7feXc0d2uiEIxLMtrvrwbQiQCsCASVbeKBgd/tLkVyyAMswpbchVzLPUM4X3GtFh7iODlIJU5WAPsvAJU+eo+NK+P8UNqFtorXCrMudSV0KgSAQZZmUV9Z41YtNk1zOQxyI7Z2du7zg/OAYZSZ0AHKkx8DjL1riDWaAPHsOQCHOxMZGnKFzzESBGsmicFiluKtxM2U7ZlKkjqFYAx50Co8RxzTcf67xH1zTDhNsZW+t/StDYsgO5ifG/87UTws+Ftva/pWp3G9UKcCfAs7R+Zrbf4baX7wXbIvLoTWKwj+BRy/wCTW1/w0b5e70yLz8zReSX/ACbBuF2GZ8wMzJYvuWVlIkGTlUkehHSvsdw/DQ3eMAHhCC8DM1s2xHRzblR5UHc7PZ3dzd1drjj5MaFwqqDr4sqKRykmfKh8b2dZ3CEzbIuubmRYV3QWwiIWJDZWuNJBUFttKsyQzbg+HYlGTKoZjGaEJujxhVB08x+8aqucJRnR1OQLcL5SVJNxmiQDIIYDKB5zuK+wnZxVbOXzQbpWUGhcKqEknxZEUCfnHXShz2YKqpW4XZMhAyRn7pHyAeLRjdfvCf3QOhosKobX8BavOXbxFQAYfSFbOAwB69elD2OH2UuK6e1ETnmQxYiROslmj1qnhfBWXCvabKruArOAxzqqLbVyuYFGyqNA5g6yZoe/2WJM/tBVpkFbaghhYe1bbRoJQuXGm52mCBMCy5gbRuMLxztedipVoVBbRQAejCN+rUbe4VZLZmkMWUg94QcyIbYjXfKSun0qrbs/mKOXWVCwFtALowLwCxMsFQSSYjnVeK4dduYsXCoNtcsSToyLcZLog+I57kG2VGwObSKALsLwS0jIbTFQmTQOSGFtCqJExEGfhV3G71ruSLslHZEyqdXNx1thQOYJeCOk0rTswFVES9lCWgg8EnvBvdnMNW5jXyIq4dnmBUi6AEZGA7qRntq0bv7Odu8jeRqYoArPDsK157WS5nBaWzNE5UZsrToNF94ima8JtZxcg5wyvmLEnMiNbWeoCu+nViaNB0E6mNTESeZjlrXCkjSk3eBrCsFbhVrvO+1zzPtHLOUKTl2nKAPdQ9zg9hnZyrFmL5vEYIuKiMsfRIRNP3aYW7dSgUvwABuDWTOh1fP7R8Lfu/RHlVX/AEdO8BPsC6bwEtPelWRpJ0yw505zTWa4oppg0A3+FrduM90yPCFVSRlCtn1PUnf0qv8A6JYhQVJyFcpzGQFDqqz9ELcuCP3jzpjmr5RNCYmsiwcEs9Gkp3ZOcyyHk3U8vSmKtqBy0j0qRWK+UAa0w4PFcQCXuCN7j/ztRPDFOVtPnf0rQty78pc+u/8AO1HcOJIb63L6q1jmzobVIUYMju1iZ/5NaTsVxazh7txrzQGRQIHOSTWY4e3gX++Zprwuwrkyk/K2F3bRbjOGGh8hT4YsNHoZ7aYID2z9k1Fe2mEj22+ya87HDy6oR4QUtk+FjBNoOxJJnUzVV/h7KRbBPiMAxptJIg7KDP6VW5keNHpC9tMLoc7R9U1Ju2+Dj/uH7Jrzo4FC2ZHItMiOpALgS3duJbIYVvFMaBhpXLOAXNlnvBFsnwsBFw6ZWDSdtyKLYbUej2+2+DG9xh/tNff5zwhP/dJ6eE/pXmduzNhSFJY3Ms6zBtloInKNQKsucPK5pc+HRvA5hirx/tOT2v3hStjUI9no57cYMad432TXP88YMf8AkP2TXlhuazE6aevn51WP/dG5sa00j1Re2+D+mfsmr7nbjBcrp+ya8jPXeahTsWxHrR7b4PfOfsmuf54wnJ2+ya8sBnlyqLb6eVK2NwTwetW+2eDjW4fsmof51wf+ofsmvLSNRr1qRUbnl/e1TvY/EqPUD21wn0m+yaq/zvhSfbb7JrzJbs6eRioF9Z56fcI60KTH40z1Ru2WD/1T9k1wdtcF/qH7Jryy5oNttR6VTvrVp2Q9NI9aftvgv9U/ZNWL22wX+ofsmvIRVq+f9zQ2Gyy93zO53BdyD5FmI+6mvB28LfW/pWlNtht50z4be8LfW/pWpsqkJMI3gX0/Oj8GbeU52IMiNSNJ3Eb+/agMNb8C6nby619cYrsfiB+lVTBSGSragSWnpJ1g/pViLZKj2lYkgnMYAM+LT3fGs5dxbg6H7h+lfNjHHPpyHOpplNoflbSyFutlIAI8gZ9w0G1DgWwzfKEgbbgsMhIE8vFArPvxK4WMkfAVYMW/X7h506sjg0It28mlwgxtyJgGI9ZHwrhs2jALFR4QdySoIOo5RyA00pGMW4AMjboK6ca+pn7h1HlUl4Hxt2gRLyD7XIjw6RHKaFvWkAlXzabfcB99KLuNfqNB0FXNi3015TsP0oQNoLJ5eoqIEUE2Nedx8BVd3HP15dB1qkJjFVqZj8PhSZMc87j4DpVbcQfqPgOVFE7qH4YDxH3DyqNy5JPQ7RSi5jngGR8BVT8QuQDP3Cls7Kc+hy45dK+IGn4+k0oTHPpqPgKv/am6/cP0p0JSGTkctzvVYNLUxjnmPgP0qD45yd9vIfpTSoTl2Njrv7qlm5fGk6Y1zGo+Aqy7jHEajXyFOgsbBqbcJQZT9b+layox77ab9BRNnjd1JAynWdR5DpQkJyP/2Q==\" /></p>\r\n','2017-10-30 15:34:38'),(3,10,'Deysi','deysirose@gmail.com','<p>My comment is more important</p>\r\n','2017-10-30 15:02:41'),(4,10,'BRUNO GONCALVES DOS SANTOS','brunogoncalves.santos@gmail.com','<p>Mais um coment&aacute;rio sobre esse dog safado</p>\r\n','2017-10-30 15:03:35'),(5,10,'Deysielle depaula','deysirose@gmail.com','<p>Mais um coment&aacute;rio meu</p>\r\n','2017-10-30 15:05:33'),(6,10,'Deysielle depaula','deysirose@gmail.com','<p>Mais um coment&aacute;rio meu</p>\r\n','2017-10-30 15:06:47');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Loki encontra novo hobbie','loki-encontra-novo-hobie','Comer chinelos velhos'),(2,'Deysi ganha chinelos','deysi-ganha-chinelos','Deysi ganhou um par de chinelos do Cartoon Network');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'New form of life discovered','new-form-of-life-discovered','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor pretium placerat. Etiam sit amet est facilisis, eleifend ligula quis, ultricies lacus. Etiam ornare libero quam, in efficitur turpis dapibus a. Aliquam erat volutpat. Pellentesque vitae nunc sit amet leo cursus efficitur at vitae magna. Duis rutrum luctus sollicitudin. Nam nec nisi aliquam, efficitur est consectetur, porttitor nisi. Nulla maximus dolor quis dolor dapibus, auctor fringilla tortor posuere. Quisque accumsan dolor vel magna dictum sagittis. Aenean sed rutrum magna. Sed ac ultricies odio, et aliquam nunc. Vestibulum a leo et elit tincidunt lobortis eget vel turpis. Pellentesque lobortis nibh at semper aliquet. Morbi vitae egestas magna, sed rutrum dui.\n\nFusce erat mi, tincidunt in fermentum sit amet, rutrum sit amet justo. Integer interdum, lacus sed luctus porttitor, erat ex ornare ipsum, vitae sodales quam dolor sed erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras faucibus aliquam est eget laoreet. Nullam lacus purus, mollis quis feugiat at, laoreet ac augue. Maecenas ut libero auctor, feugiat nunc nec, blandit ex. Donec faucibus augue ex, vitae varius mauris semper vel. Curabitur lacinia orci vel mi cursus sodales. Donec ullamcorper neque euismod euismod sagittis. Etiam ac porttitor nibh. Nullam arcu risus, posuere sit amet eleifend quis, dictum in erat. Morbi et odio nunc. Integer vitae ultricies nunc. Nullam sed neque varius, fermentum tortor nec, mattis velit. Aenean aliquam nisi vitae volutpat hendrerit. Donec volutpat, felis ac porta lobortis, elit nunc maximus tellus, quis sollicitudin eros nulla id enim.',NULL,'2017-10-28 14:16:34',1,1),(2,'Celular inventado','celular-inventado','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor pretium placerat. Etiam sit amet est facilisis, eleifend ligula quis, ultricies lacus. Etiam ornare libero quam, in efficitur turpis dapibus a. Aliquam erat volutpat. Pellentesque vitae nunc sit amet leo cursus efficitur at vitae magna. Duis rutrum luctus sollicitudin. Nam nec nisi aliquam, efficitur est consectetur, porttitor nisi. Nulla maximus dolor quis dolor dapibus, auctor fringilla tortor posuere. Quisque accumsan dolor vel magna dictum sagittis. Aenean sed rutrum magna. Sed ac ultricies odio, et aliquam nunc. Vestibulum a leo et elit tincidunt lobortis eget vel turpis. Pellentesque lobortis nibh at semper aliquet. Morbi vitae egestas magna, sed rutrum dui.\n\nFusce erat mi, tincidunt in fermentum sit amet, rutrum sit amet justo. Integer interdum, lacus sed luctus porttitor, erat ex ornare ipsum, vitae sodales quam dolor sed erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras faucibus aliquam est eget laoreet. Nullam lacus purus, mollis quis feugiat at, laoreet ac augue. Maecenas ut libero auctor, feugiat nunc nec, blandit ex. Donec faucibus augue ex, vitae varius mauris semper vel. Curabitur lacinia orci vel mi cursus sodales. Donec ullamcorper neque euismod euismod sagittis. Etiam ac porttitor nibh. Nullam arcu risus, posuere sit amet eleifend quis, dictum in erat. Morbi et odio nunc. Integer vitae ultricies nunc. Nullam sed neque varius, fermentum tortor nec, mattis velit. Aenean aliquam nisi vitae volutpat hendrerit. Donec volutpat, felis ac porta lobortis, elit nunc maximus tellus, quis sollicitudin eros nulla id enim.',NULL,'2017-10-28 14:17:35',3,8),(3,'Bruno precisa aprender bootstrap/bootswatch','bruno-precisa-aprender-bootstrap-bootswatch','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor pretium placerat. Etiam sit amet est facilisis, eleifend ligula quis, ultricies lacus. Etiam ornare libero quam, in efficitur turpis dapibus a. Aliquam erat volutpat. Pellentesque vitae nunc sit amet leo cursus efficitur at vitae magna. Duis rutrum luctus sollicitudin. Nam nec nisi aliquam, efficitur est consectetur, porttitor nisi. Nulla maximus dolor quis dolor dapibus, auctor fringilla tortor posuere. Quisque accumsan dolor vel magna dictum sagittis. Aenean sed rutrum magna. Sed ac ultricies odio, et aliquam nunc. Vestibulum a leo et elit tincidunt lobortis eget vel turpis. Pellentesque lobortis nibh at semper aliquet. Morbi vitae egestas magna, sed rutrum dui.\n\nFusce erat mi, tincidunt in fermentum sit amet, rutrum sit amet justo. Integer interdum, lacus sed luctus porttitor, erat ex ornare ipsum, vitae sodales quam dolor sed erat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras faucibus aliquam est eget laoreet. Nullam lacus purus, mollis quis feugiat at, laoreet ac augue. Maecenas ut libero auctor, feugiat nunc nec, blandit ex. Donec faucibus augue ex, vitae varius mauris semper vel. Curabitur lacinia orci vel mi cursus sodales. Donec ullamcorper neque euismod euismod sagittis. Etiam ac porttitor nibh. Nullam arcu risus, posuere sit amet eleifend quis, dictum in erat. Morbi et odio nunc. Integer vitae ultricies nunc. Nullam sed neque varius, fermentum tortor nec, mattis velit. Aenean aliquam nisi vitae volutpat hendrerit. Donec volutpat, felis ac porta lobortis, elit nunc maximus tellus, quis sollicitudin eros nulla id enim.',NULL,'2017-10-28 14:48:34',2,4),(7,'My Updated Post Again','My-Newest-Post','<p style=\"font-style: italic;\"><s>Some new things here. With even more things. Lorem</s> ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan rutrum quam, non placerat eros dapibus id. Fusce malesuada luctus volutpat. Morbi gravida accumsan finibus. Donec laoreet sodales justo, non pretium augue. Cras eu aliquet ex. In elementum ligula non lacus laoreet euismod. Ut mauris quam, hendrerit eu accumsan et, commodo et justo. Donec neque neque, varius id massa ut, consectetur faucibus velit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean eleifend justo eu velit placerat, id aliquet enim lacinia. Nullam porta sapien in dui placerat, sed volutpat velit mollis. Maecenas tempus pretium suscipit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse maximus convallis odio at luctus. Donec ultrices felis id tellus porttitor, sed porta orci interdum. In justo neque, auctor eu tincidunt eu, commodo quis neque. Nulla nec mattis metus. Sed vitae malesuada elit. Nulla facilisi. Ut venenatis mi non ligula pellentesque, eu accumsan sapien facilisis. Etiam accumsan sapien mauris, id ullamcorper ligula finibus sit amet. Sed molestie nunc urna, at tincidunt lectus efficitur id. Donec pulvinar tellus dolor, ac sagittis lectus fringilla nec. Suspendisse tempor posuere libero, nec suscipit diam tempor ac. Aenean gravida malesuada dolor, a egestas sapien congue nec. Maecenas ipsum arcu, rutrum et sapien sed, facilisis facilisis orci. Vivamus est sapien, luctus vitae fringilla id, bibendum vel justo. Donec lacinia, nulla ut lacinia vulputate, dolor enim sollicitudin lorem, in feugiat eros felis vitae mi. Suspendisse lacinia est ex, quis posuere tellus vestibulum vitae. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc volutpat nulla sit amet libero luctus, a laoreet felis sollicitudin. Phasellus malesuada nisi nec lorem euismod gravida a interdum leo. Praesent ex est, venenatis vel velit sed, malesuada viverra ex. Aliquam eget eleifend justo. Curabitur semper ipsum sit amet sapien mattis efficitur. Aliquam ultricies ipsum nisi, non pulvinar nulla venenatis vel. Nulla facilisi. Praesent malesuada eu nisl id tincidunt. Mauris eget quam scelerisque, aliquet ligula quis, dignissim lacus. Aliquam scelerisque, leo nec cursus blandit, lorem elit imperdiet tortor, non pharetra ex odio a justo. Fusce sit amet erat et erat varius consectetur. Quisque quis nunc ut sapien porttitor dignissim sed quis ex. Nulla efficitur mauris at erat vulputate aliquet. Nulla sed blandit velit. Ut ac lorem sed mauris placerat tincidunt. Ut ut enim tincidunt, efficitur mi ut, fringilla elit. Proin gravida dui quis eros aliquet pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus mauris nunc, lacinia et sem id, sagittis molestie massa. Maecenas tincidunt molestie arcu, sit amet commodo tellus accumsan eget. Duis placerat leo est, ac bibendum dolor sagittis sit amet. Ut vehicula mi quis laoreet aliquet. Nulla ac nulla blandit, vestibulum risus et, varius augue. Duis gravida ipsum a imperdiet ultricies. Quisque faucibus molestie lacinia.</p>\r\n',NULL,'2017-10-28 21:24:22',4,3),(9,'Imagem','Imagem','<p><img alt=\"design responsivo\" src=\"http://jovemadministrador.com.br/wp-content/uploads/2015/12/responsive_final.png\" style=\"height:275px; width:590px\" /></p>\r\n',NULL,'2017-10-28 22:54:33',2,1),(10,'Doge é um cachorro safado','Doge-um-cachorro-safado','<p>Hoje descobri que o <strong>Loki </strong>&eacute; um cachorro mega safado.</p>\r\n\r\n<p><img alt=\"\" src=\"http://cdn1-www.dogtime.com/assets/uploads/gallery/labrador-retriever-dog-breed-pictures/labrador-retriever-dog-pictures-1.jpg\" style=\"height:452px; width:680px\" /></p>\r\n','noimage.jpg','2017-10-29 13:40:04',3,1),(18,'Nova carta de MAGIC','we','<p>A imagem de thumbnail foi inserida durante a cria&ccedil;&atilde;o do post na aplica&ccedil;&atilde;o.</p>\r\n\r\n<p>&nbsp;</p>\r\n','Bishop-of-Rebirth-Ixalan-MtG-Art.jpg','2017-10-29 14:42:46',1,1),(19,'Guardiões da Galáxia 2 em Exibição','Guardies-da-Galxia-2-em-Exibio','<p>Em exibi&ccedil;&atilde;o novo filme dos Guardi&otilde;es da Gal&aacute;xia.&nbsp;</p>\r\n','4327607-hulk_and_the_guardians_of_the_galaxy___movie_by_worldbreakerhulk-d8ccbya.jpg','2017-10-30 20:01:18',4,7),(20,'Bruno  ama o bozi','Bruno-no-ama-o-bozi','<p>Bruno &nbsp;ama o bozi dele</p>\r\n','perfil.jpg','2017-10-30 22:13:28',11,10);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'BRUNO GONCALVES DOS SANTOS','22765080','brunogoncalves.santos@gmail.com','GSBRUNO','af788365ec2f6b5d5e8bb22deddef469','2017-10-30 16:52:21'),(2,'Deysi','23231','deysirose@gmail.com','deysi','af788365ec2f6b5d5e8bb22deddef469','2017-10-30 16:59:31'),(3,'BRUNO GONCALVES DOS SANTOS','22765080','brunogoncalves.santos@gmail.com','Bruno','202cb962ac59075b964b07152d234b70','2017-10-30 17:01:41'),(4,'Deysielle depaula','22765080','brunogoncalves.santos@gmail.com','ew','7815696ecbf1c96e6894b779456d330e','2017-10-30 17:03:34'),(5,'BRUNO GONCALVES DOS SANTOS','22765080','brunogoncalves.santos@gmail.com','brunotb','202cb962ac59075b964b07152d234b70','2017-10-30 17:39:41'),(6,'BRUNO GONCALVES DOS SANTOS','22765080','brunogoncalves.santos@gmail.com','brunotg','202cb962ac59075b964b07152d234b70','2017-10-30 17:39:55'),(7,'brunos','1232','12321@ewqeq.com.br','brunos','202cb962ac59075b964b07152d234b70','2017-10-30 18:41:12'),(8,'loki','3321','loki@dog.com','loki','202cb962ac59075b964b07152d234b70','2017-10-30 20:29:09'),(9,'Ball','22765080','bss@gmail.com','BallSantos','202cb962ac59075b964b07152d234b70','2017-10-30 21:45:00'),(10,'Deysielle depaula','22765080','deysidepaula88@gmail.com','deysiellep','e3a72c791a69f87b05ea7742e04430ed','2017-10-30 22:11:48'),(11,'BRUNO GONCALVES DOS SANTOS','22765080','brunogoncalves.santrs@gmail.com','brunosenha','9928d26069ee99395a33e214e9e4ec22','2017-10-30 22:22:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-30 20:49:18
